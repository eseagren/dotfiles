#! /bin/bash

# Check running on Mac OSX
if [ $(uname) != "Darwin" ]; then
    echo_error "Incorrect operating system. For Mac OSX only."
    exit 1
fi

# Install Homebrew
if test ! "$(command -v brew)"; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Make sure we're using latest Homebrew
brew update

# Upgrade any already-installed formulae
brew upgrade

# ------------------------------------------------------
# Applications
# ------------------------------------------------------

brew cask install "firefox"
brew cask install "google-chrome"
brew cask install "fontbase"
brew cask isntall "iterm2"
brew cask isntall "visual-studio-code"
brew cask install "vlc"

# ------------------------------------------------------
# Tools
# ------------------------------------------------------

brew install "exiftool"
brew install "ffmpeg"
brew install "git"
brew install "htop"


# Remove outdated versions from the cellar
brew cleanup