" Vi Improved configuration file

set nocompatible                " Don't make Vim vi-compatible
                                " this must be first

if has('autocmd')
    filetype plugin indent on
    "           │     │    └──────── Enable file type detection.
    "           │     └───────────── Enable loading of indent file.
    "           └─────────────────── Enable loading of plugin files.
endif


"
" Settings
"

set encoding=utf-8
set fileencoding=utf-8

set nobackup
set history=200
set confirm                     " confirm :q in case of unsaved changes

syntax enable                   " enable syntax highlighting
set number                      " show line numbers
set showcmd                     " show me what I'm typing
set showmatch                   " highlight matching braces
set showmode                    " show insert/replace/visual mode
set wrap

set backspace=indent            " ┐
set backspace+=eol              " │ Allow `backspace`
set backspace+=start            " ┘ in insert mode.

set autoindent                  " Copy indent to the new line.
set expandtab                   " fill tabs with spaces
set nojoinspaces                " no extra space after '.' when joining lines
set shiftwidth=4                " set indentation depth to 4 columns
set softtabstop=4               " backspacing over 4 spaces like over tabs
set tabstop=4                   " set tabulator length to 4 columns
set textwidth=79                " wrap lines automatically at 80th column
