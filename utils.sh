#! /bin/bash

#
# A collection of utility functions to be sourced
# by install/bootstrap scripts for dotfiles.
#

red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 11)
magenta=$(tput setaf 5)
cyan=$(tput setaf 5)
gray=$(tput setaf 7)
color_reset=$(tput sgr0)


function echo_error() {
  printf "${red}%s${color_reset}\n" "[ERROR] $1"
}

function echo_warning() {
  printf "${yellow}%s${color_reset}\n" "[WARN]  $1"
}

function echo_info() {
  printf "${gray}%s${color_reset}\n" "[INFO]  $1"
}

function echo_info_r() {
  # print info statement with \r at end
  printf "${gray}%s${color_reset}\r" "[INFO]  $1"
}

function echo_green() {
  printf "${green}%s${color_reset}\n" "[ERROR] $1"
}