# ~/.aliases
# [will be sourced by bash_profile]

alias a='alias'

# ------------------------------------------------------
# Navigation
# ------------------------------------------------------
a u='cd ..'
a uu='cd ../..'
a uuu='cd ../../..'

# ------------------------------------------------------
# Safety First!
# ------------------------------------------------------
a rm='rm -i -v'   # force interactive and verbose
a cp='cp -i'      # copy file interactive
a mv='mv -i'      # move file interactive


# ------------------------------------------------------
# Files & Folders (customize `ls`)
# ------------------------------------------------------

# Detect which `ls` flavor is in use
if ls --color > /dev/null 2>&1; then  # GNU `ls`
	colorflag="--color"
else  # OS X `ls`
	colorflag="-G"
fi

# List all files colorized in long format, excluding . and ..
a l='ls -lhAF ${colorflag}'  
a ll='l'  # habit

# List only directories
a lsd='ls -lF ${colorflag} | grep --color=never "^d"'


# ------------------------------------------------------
# Shortcuts
# ------------------------------------------------------
a cask='brew cask'
a which='type'
a where='type'

# ------------------------------------------------------
# Commands
# ------------------------------------------------------

# VLC commandline
a vlc='/Applications/VLC.app/Contents/MacOS/VLC'
a vlc-yt-convert='vlc -I dummy "$1" --sout="#transcode{vcodec=h264,vb=4096,acodec=mp4a,ab=192,channels=2,deinterlace}:file{dst=yt-output.mp4}" vlc://quit'

# Convert .heic -> .jpg
a heic2jpg="find . -type f -iname '*.heic' -exec sh -c 'mogrify -format jpg \"{}\"' \;"

# Add an "alert" alias for long running commands.  Use like so:
# > sleep 10; alert
a alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
