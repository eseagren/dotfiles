settings.json

Settings and preferences for the Windows terminal. This file should be linked
to the following path:
C:\Users\<user>\AppData\Local\Packages\Microsoft.WindowsTerminal_<UNIQUE_ID>\LocalState
where 'Microsoft.WindowsTerminal_<UNIQUE_ID>' contains a unique ID string.
